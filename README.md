Designing a PiHat UPS that replaces normal Pi power with wall power that doubles as a battery charger for a Li-Ion battery when plugged in,
 and supplies battery power to the Pi when there is no input power.


This project will feature a power supply subsystem that handles supplying the RPI and charging the battery, and two interfacting subsystems, one with LED's to visually show status, and another which converts battery status into an anologue voltage for the RPI.
